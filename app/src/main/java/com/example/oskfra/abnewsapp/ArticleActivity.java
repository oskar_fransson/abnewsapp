package com.example.oskfra.abnewsapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.oskfra.abnewsapp.models.Article;

/**
 * An activity used to display the articles.
 */
public class ArticleActivity extends AppCompatActivity {

    private TextView tvTitle;
    private TextView tvIngress;
    private TextView tvText;
    private ImageView ivArticleTop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        tvTitle = (TextView)findViewById(R.id.tv_article_title);
        tvIngress = (TextView)findViewById(R.id.tv_article_ingress);
        tvText = (TextView)findViewById(R.id.tv_article_text);
        ivArticleTop = (ImageView)findViewById(R.id.iv_article_top);

        Article article = null;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            try {
                article = new Article(extras.getInt("articleImage"),extras.getString( "articleTitle"),
                        extras.getString("articleIngress"), extras.getString( "articleText"));
            }catch (Exception e){
                Log.d("ArticleActivity", "Could Not Create Article, error " + e);
            }
        }

        if (article != null){
            setUpView(article);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_article_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_text_increase:
                increaseTextSize();
                break;
            case R.id.action_text_decrease:
                decreaseTextSize();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpView(Article article) {
        if (article.getImagePath() != -1) {
            Glide.with(this).load(article.getImagePath()).into(ivArticleTop);
        }
        tvTitle.setText(article.getTitle());
        tvIngress.setText(article.getIngress());
        tvText.setText(article.getText());

    }

    private void increaseTextSize(){
        tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, (tvTitle.getTextSize() + 2));
        tvIngress.setTextSize(TypedValue.COMPLEX_UNIT_PX,  (tvIngress.getTextSize() + 2));
        tvText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (tvText.getTextSize() + 2));
    }
    private void decreaseTextSize(){
        tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, (tvTitle.getTextSize() - 2));
        tvIngress.setTextSize(TypedValue.COMPLEX_UNIT_PX,  (tvIngress.getTextSize() - 2));
        tvText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (tvText.getTextSize() - 2));
    }
}
