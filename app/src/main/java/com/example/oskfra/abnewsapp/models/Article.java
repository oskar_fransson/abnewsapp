package com.example.oskfra.abnewsapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Article implements Parcelable{

    private int imagePath;
    private String text;
    private String title;
    private String ingress;

    public Article(
            int imageUrl,
            String title,
            String ingress,
            String text) {

        this.imagePath = imageUrl;
        this.title = title;
        this.text = text;
        this.ingress = ingress;
    }

    /**
     *
     * @return -1 if no image.
     */
    public int getImagePath() {
        return imagePath;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    public String getIngress() {
        return ingress;
    }

    protected Article(Parcel in) {
        imagePath = in.readInt();
        text = in.readString();
        title = in.readString();
        ingress = in.readString();
    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(imagePath);
        dest.writeString(text);
        dest.writeString(title);
        dest.writeString(ingress);
    }
}
