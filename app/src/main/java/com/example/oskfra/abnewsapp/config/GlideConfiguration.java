package com.example.oskfra.abnewsapp.config;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.module.GlideModule;

/**
 * Override the default DecodeFormat to PREFER_ARGB_8888, this will improve the
 * image quality but consumes more memory. For tests See > https://inthecheesefactory.com/blog/get-to-know-glide-recommended-by-google/en
 */

public class GlideConfiguration implements GlideModule {
 
    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        builder.setDecodeFormat(DecodeFormat.PREFER_ARGB_8888);
    }
 
    @Override
    public void registerComponents(Context context, Glide glide) {
    }
}