package com.example.oskfra.abnewsapp.util;

import android.content.Context;
import android.text.Layout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.flexbox.FlexboxLayout;

/**
 * Created by oskfra on 18/06/16.
 */
public class UIFactory {

    public static void addButtonToLinearLayout(Context context, ViewGroup layout, LinearLayout.LayoutParams lllParams, String btnText, View.OnClickListener clickListener) {
        Button btn = new Button(context);
        btn.setTransformationMethod(null);//Stop button from capitalize the text
        btn.setOnClickListener(clickListener);
        btn.setText(btnText);
        layout.addView(btn, lllParams);
    }

}
