package com.example.oskfra.abnewsapp.Adapter;

import android.view.View;

/**
 * Created by oskfra on 07/06/16.
 * A simple interface used to implement a click event for the items in the recyclerview.
 */
public interface ClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
