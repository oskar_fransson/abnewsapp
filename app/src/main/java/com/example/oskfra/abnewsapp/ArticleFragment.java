package com.example.oskfra.abnewsapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.oskfra.abnewsapp.Adapter.BorderItemDecoration;
import com.example.oskfra.abnewsapp.Adapter.ClickListener;
import com.example.oskfra.abnewsapp.Adapter.MainRecycleViewAdapter;
import com.example.oskfra.abnewsapp.Adapter.RecyclerTouchListener;
import com.example.oskfra.abnewsapp.models.Article;
import com.example.oskfra.abnewsapp.util.UIFactory;
import com.google.android.flexbox.FlexboxLayout;

import java.util.ArrayList;

/**
 * This is the fragment used by all sections in the app. Since they all
 * look the same except for the dataset used only one fragment is necessary.
 * It takes a list in the constructor that will be used to populate a
 * recyclerview.
 */

public class ArticleFragment extends Fragment {
    public static String SEARCH_TERM = "SEARCH_TERM";
    public static String SAVED_SEARCH_TERMS = "SAVED_SEARCH_TERMS";
    public static String RECYCLER_VIEW_FIRST_INIT_LIST =  "RECYCLER_VIEW_FIRST_INIT_LIST";

    private RecyclerView recyclerView;
    private MainRecycleViewAdapter recyclerViewAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private FlexboxLayout fLayout;
    private View view;

    private String searchTerm ="";
    private SearchView searchView;
    private ArrayList<Article> dataSource = new ArrayList<>();

    public static ArticleFragment newInstance(ArrayList<Article> dataSource) {
        Bundle extras = new Bundle();
        extras.putParcelableArrayList("dataSource", dataSource);

        ArticleFragment fragment = new ArticleFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Get and set contructor data
        Bundle constructorData = getArguments();
        if (constructorData != null) {
            dataSource = (ArrayList<Article>) constructorData.getSerializable("dataSource");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_article, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.rvArticle);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerViewAdapter = new MainRecycleViewAdapter(getContext(), dataSource);
        fLayout = (FlexboxLayout)view.findViewById(R.id.llArticleSearchTerms);

        setUpDefaultRecyclerView();

        //call the fragments onCreateOptionsMenu, (should be set last in onCreateView or in later cycle)
        setHasOptionsMenu(true);
        return view;
    }

    private void setUpDefaultRecyclerView(){
        //Add a click handeler to the items in the Recycler view
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                view.setPressed(true);

                Article article = recyclerViewAdapter.getItem(position);

                Intent intent = new Intent(getContext(), ArticleActivity.class);
                Bundle extras = new Bundle();
                extras.putInt("articleImage", article.getImagePath());
                extras.putString("articleTitle",article.getTitle());
                extras.putString("articleIngress",article.getIngress());
                extras.putString("articleText",article.getText());
                intent.putExtras(extras);
                getContext().startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(getContext(), " Not Yet Implemented!", Toast.LENGTH_SHORT).show();
            }
        }));

        recyclerView.setHasFixedSize(true); //to improve performance

        //Add a bottom border using a BorderItemDecoration
        recyclerView.addItemDecoration(new BorderItemDecoration(getContext(), LinearLayoutManager.VERTICAL));

        recyclerView.setAdapter(recyclerViewAdapter);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        final MenuItem searchItem = menu.findItem(R.id.action_Search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        //If there is a searchterm than add it to the searchView. Used when screen rotate.
        if (searchTerm != null && !searchTerm.equals("")){
            MenuItemCompat.expandActionView(searchItem);
            searchView.setQuery(searchTerm, false);
            searchTerm = "";//Clean SearchTerm for slider. We only want to restore the SearchView text on scren rotate
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Make a buttom of the search term and add it to the fragment. When click it removes the search filter.
                addSearchTermButton(query);
                MenuItemCompat.collapseActionView(searchItem);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void addSearchTermButton(final String searchTerm) {
        recyclerViewAdapter.addSearchTerm(searchTerm);
        LinearLayout.LayoutParams lllParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        String btnText = searchTerm + " [X]";
        UIFactory.addButtonToLinearLayout(getContext(), fLayout, lllParams, btnText, new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                recyclerViewAdapter.removeSearchTerm(searchTerm);
                fLayout.removeView(v);
                recyclerViewAdapter.filterArticlesFromSearchTerms();
            }
        });
        recyclerViewAdapter.filterArticlesFromSearchTerms();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (searchView != null) {
            outState.putString(SEARCH_TERM, searchView.getQuery().toString());
            outState.putStringArrayList(SAVED_SEARCH_TERMS, recyclerViewAdapter.getSavedSearchTerms());
            outState.putParcelableArrayList(RECYCLER_VIEW_FIRST_INIT_LIST, recyclerViewAdapter.getOriginalList());
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {

            searchTerm = savedInstanceState.getString(SEARCH_TERM);

            ArrayList<String> searchTerms = savedInstanceState.getStringArrayList(SAVED_SEARCH_TERMS);
            if (searchTerms != null && dataSource != null) {
                dataSource = savedInstanceState.getParcelableArrayList(RECYCLER_VIEW_FIRST_INIT_LIST);
                recyclerViewAdapter.setOriginalList(dataSource);
                for (String term : searchTerms) {
                    addSearchTermButton(term);
                }
            }


        }
    }


}
