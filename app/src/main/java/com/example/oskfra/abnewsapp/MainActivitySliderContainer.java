package com.example.oskfra.abnewsapp;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.example.oskfra.abnewsapp.models.Article;
import com.example.oskfra.abnewsapp.models.Data;

import java.util.ArrayList;

/**
 * This is the activity that houses the apps 3 tabs (sport, news and entertainment).
 */

public class MainActivitySliderContainer extends AppCompatActivity {

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity_slider_container);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SetupViewPager();

    }

    private void SetupViewPager() {
        // Set up the ViewPager with the sections adapter.
        viewPager = (ViewPager) findViewById(R.id.main_activity_viewpager);
        viewPager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
        TabLayout tabLayout = (TabLayout) findViewById(R.id.main_activity_sliding_tabs);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * This class configures the sliding tabs (The titles and what fragment that will be show
     * in what section).
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        private String tabTitles[] = new String[]{"Sport", "News", "Entertainment"};

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ArticleFragment.newInstance((ArrayList<Article>)Data.getSportArticles());
                case 1:
                    return ArticleFragment.newInstance((ArrayList<Article>)Data.getNewsArticles());
                case 2:
                    return ArticleFragment.newInstance((ArrayList<Article>)Data.getEntertainmentArticles());
                default:
                    return ArticleFragment.newInstance((ArrayList<Article>)Data.getSportArticles());
            }
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

    }
}
