package com.example.oskfra.abnewsapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.oskfra.abnewsapp.R;
import com.example.oskfra.abnewsapp.models.Article;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Created by Alter on 2016-05-22.
 * MainRecycleViewAdapter maps the data from an Article object to a row (recycler_row_item.xml)
 * with the help of a RecyclerViewHolder (defined as a static class in this file).
 * The rows will be added to a RecyclerView used in each section(sport, news, entertainment).
 *
 * I only need one RecycleView since the model is the same for sports, news and entertainment.
 */

public class MainRecycleViewAdapter extends RecyclerView.Adapter<MainRecycleViewAdapter.RecyclerViewHolder> {

    private Context context;

    private ArrayList<Article> articleList = new ArrayList<>();
    private ArrayList<Article> originalList = new ArrayList<>();

    private ArrayList<String> savedSearchTerms = new ArrayList<>();

    public MainRecycleViewAdapter( Context context, ArrayList<Article> articleList) {
        this.originalList = new ArrayList<>(articleList);
        this.articleList = articleList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row_item, parent, false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    public Article getItem(int position) {
        return articleList.get(position);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Article article = articleList.get(position);

        holder.rvTvTitle.setText(article.getTitle());
        int image = article.getImagePath();
        if (image != -1) {
            Glide.with(context).load(image).into(holder.rvIvThumbnail);
        }else{
            Glide.with(context).load(R.drawable.no_image).into(holder.rvIvThumbnail);
        }

    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    //This class just defines the data in our recycler_row_item.xml.
    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        private TextView rvTvTitle;
        private ImageView rvIvThumbnail;
        public RecyclerViewHolder(View view) {
            super(view);
            rvIvThumbnail = (ImageView)view.findViewById(R.id.rv_iv_thumbnail);
            rvTvTitle = (TextView)view.findViewById(R.id.rv_tv_title);
        }
    }

    /*
    CUSTOM METHODS BELOW
    */
    //These two getters are needed for the fragment that uses this adapter so its state can be stored.
    public ArrayList<Article> getOriginalList(){
        return originalList;
    }
    public void setOriginalList(ArrayList<Article> orig){
        this.originalList = orig;
    }
    public ArrayList<String> getSavedSearchTerms(){
        return savedSearchTerms;
    }

    public void addSearchTerm(String searchTerm) {
        savedSearchTerms.add(searchTerm);
    }
    public void removeSearchTerm(String searchTerm) {
        savedSearchTerms.remove(searchTerm);
    }

    //update the adapter with data from a new list
    public void updateAdapter(ArrayList<Article> list){
        if (list != null) {
            articleList.clear();
            articleList.addAll(list);
            notifyDataSetChanged();
        }else {
            Toast.makeText(context, "updateAdapter list null", Toast.LENGTH_SHORT).show();
        }
    }


    //Filter the adapter with the specified searchTerms (savedSearchTerms)
    public void filterArticlesFromSearchTerms(){
        ArrayList<Article> result = new ArrayList<>();
        for(Article ar : originalList){
            String[] splitTitle = ar.getTitle().split(" ");
            if(Arrays.asList(splitTitle).containsAll(savedSearchTerms)){
                result.add(ar);
            }
        }
        updateAdapter(result);
    }

    /*
    END CUSTOM METHODS
    */
}
